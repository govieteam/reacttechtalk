import { Html, Sphere } from '@react-three/drei';
import { MeshProps } from '@react-three/fiber';
import React, { useRef, useState } from 'react';
import { Object3D } from 'three';

export default function HoverSphere(props: MeshProps) {
  const sphereRef = useRef<Object3D>();

  const [isHover, setHover] = useState(false);
  return (
    <Sphere
      ref={sphereRef}
      {...props}
      args={[0.5]}
      scale={isHover ? [1.2, 1.2, 1.2] : [1, 1, 1]}
      onPointerEnter={() => setHover(true)}
      onPointerOut={() => setHover(false)}>
      <meshStandardMaterial
        color={isHover ? 'yellow' : 'royalblue'}
        attach="material"
        roughness={0.2}
      />

      <Sphere args={[0.3]} position={[0.4, 0.4, 0]}>
        <meshStandardMaterial
          color={isHover ? 'yellow' : 'royalblue'}
          attach="material"
          roughness={0.3}
        />
      </Sphere>
      <Sphere args={[0.3]} position={[-0.4, 0.4, 0]}>
        <meshStandardMaterial
          color={isHover ? 'yellow' : 'royalblue'}
          attach="material"
          roughness={0.2}
        />
      </Sphere>
      <Html occlude position={[0, 1, 0]}>
        a
      </Html>
    </Sphere>
  );
}
