import { Html, OrbitControls, Stage, useProgress } from '@react-three/drei';
import { Canvas } from '@react-three/fiber';
import React, { Suspense, useState } from 'react';

import Effects from './Effects';
import HoverSphere from './HoverSphere';
import NormalSphere from './NormalSphere';
import Tokyo from './Tokyo';

function CustomLoader() {
  const { progress } = useProgress();
  return (
    <Html center>
      <span style={{ color: 'black' }}>{progress} % loaded</span>
    </Html>
  );
}

export function Stage3D() {
  return (
    <>
      <Canvas
        shadows
        dpr={window.devicePixelRatio}
        style={{ width: '100%', aspectRatio: '1.77' }}>
        <Suspense fallback={<CustomLoader />}>
          <Stage
            contactShadow
            shadows={true}
            adjustCamera
            intensity={1}
            environment="city"
            preset="upfront">
            <directionalLight />
            <NormalSphere />
            <HoverSphere position={[1.5, 0, 0]} />
            {false && <Tokyo />}
          </Stage>
        </Suspense>
        <OrbitControls zoomSpeed={2} />
        <Effects />
      </Canvas>
      <Counter amount={2} />
    </>
  );
}

interface CounterProps {
  amount: number;
}

function Counter(props: CounterProps) {
  const [sum, setSum] = useState(0);
  const handleClick = () => setSum(sum + props.amount);

  return (
    <p>
      {sum}
      <br />
      <button onClick={handleClick}>Add {props.amount}</button>
    </p>
  );
}
