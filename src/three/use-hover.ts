import { MutableRefObject, useEffect, useState } from 'react';
import { Object3D } from 'three';

export default function useHover(ref: MutableRefObject<Object3D | undefined>) {
  const [isHover, setHover] = useState(false);
  useEffect(() => {
    const obj = ref.current;
    if (!obj) return undefined;

    const handleEnter = () => setHover(true);
    const handleExit = () => setHover(false);

    obj.addEventListener('mousemove', handleEnter);
    obj.addEventListener('mouseout', handleExit);
    return () => {
      obj.removeEventListener('mousemove', handleEnter);
      obj.removeEventListener('mouseout', handleExit);
    };
  });

  return isHover;
}
