import { Sphere } from '@react-three/drei';
import { MeshProps } from '@react-three/fiber';
import React from 'react';

export default function NormalSphere(props: MeshProps) {
  return (
    <Sphere {...props} args={[0.5]}>
      {/* <meshStandardMaterial color={'royalblue'} attach="material" roughness={0.2} /> */}

      {/* <Sphere args={[0.3]} position={[0.4, 0.4, 0]}>
        <meshStandardMaterial color={'royalblue'} attach="material" roughness={0.3} />
      </Sphere> */}
    </Sphere>
  );
}
