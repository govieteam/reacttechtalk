import { useGLTF } from '@react-three/drei';
import React from 'react';

export default function Tokyo() {
  const { scene } = useGLTF('./LittlestTokyo.glb');
  return <primitive object={scene} scale={[0.01, 0.01, 0.01]} />;
}
