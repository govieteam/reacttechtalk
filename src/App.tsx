import './App.css';

import React from 'react';

import { Stage3D } from './three/stage3D';

function App() {
  return (
    <div>
      <Stage3D />
    </div>
  );
}

export default App;
